﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pedro11
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 frm=new Form2();
            frm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form4 frm = new Form4();
            frm.Show();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            textBox1.Enabled = false;
            if (checkBox5.Checked == true)
                textBox1.Text = checkBox5.Text;

            // também pode ser assim:
                // if (checkBox5.Checked==true)
                //    textBox1.Text=("Não leu nenhum destes livros");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            // Tirei estas linhas de código porque
            // Só funcionavam se eu clicasse nas labels
        }

        private void label4_Click(object sender, EventArgs e)
        {
            // if (checkBox2.Checked == true)
            //    label4.Text = checkBox2.Text;
        }

        private void label5_Click(object sender, EventArgs e)
        {
            // if (checkBox3.Checked == true)
            //    label5.Text = checkBox3.Text;
        }

        private void label6_Click(object sender, EventArgs e)
        {
            // if (checkBox4.Checked == true)
            //    label6.Text = checkBox4.Text;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            label3.Text = checkBox1.Text;
            if (checkBox1.Checked == false)
                label3.Text = ("");
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            label4.Text = checkBox2.Text;
            if (checkBox2.Checked == false)
                label4.Text = ("");
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            label5.Text = checkBox3.Text;
            if (checkBox3.Checked == false)
                label5.Text = ("");
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            label6.Text = checkBox4.Text;
            if (checkBox4.Checked == false)
                label6.Text = ("");
        }
    }
}
